package model.entity;

import lombok.*;

import java.math.BigDecimal;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Professional {

    private Integer id;
    private String name;
    private BigDecimal salary;
}
