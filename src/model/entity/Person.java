package model.entity;

import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Person {

    private String name;
    private SexEnum sex;
    private Professional professional;
}
