package application;

import model.entity.Person;
import model.entity.Professional;
import model.entity.SexEnum;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

        public static void main(String[] args) {

                List<Person> people = getPeople();

                new Professional(1, "DESENVOLVEDOR", BigDecimal.valueOf(3500.00));
                new Professional(2, "GERENTE", BigDecimal.valueOf(2900.00));
                new Professional(3, "ANALISTA", BigDecimal.valueOf(3800.00));

                System.out.println("------------------------------------------------------------");

                System.out.println("Ordem alfabetica / Ordem 3 primeiros");
                people.stream()
                        .map(Person::getName)
                        .limit(3)
                        .sorted(Comparator.naturalOrder())
                        .forEach(System.out::println);

                System.out.println();

                System.out.println("Ordem de salario / Ordem 3 primeiros");
                people.stream()
                        .map(person -> person.getProfessional().getSalary())
                        .limit(3)
                        .sorted(Comparator.naturalOrder())
                        .forEach(System.out::println);

                System.out.println("------------------------------------------------------------");

                System.out.println("Maior salario de cada profissao:");
                System.out.println();
                people.stream()
                        .filter(person -> person.getProfessional().getName().equals("DESENVOLVEDOR"))
                        .map(person -> person.getProfessional().getSalary())
                        .max(BigDecimal::compareTo)
                        .ifPresent(val -> System.out.println("Maior Salario do Desenvolvedor: " + val));

                people.stream()
                        .filter(person -> person.getProfessional().getName().equals("GERENTE"))
                        .map(person -> person.getProfessional().getSalary())
                        .max(BigDecimal::compareTo)
                        .ifPresent(val -> System.out.println("Maior Salario de Gerente: " + val));

                people.stream()
                        .filter(person -> person.getProfessional().getName().equals("ANALISTA"))
                        .map(person -> person.getProfessional().getSalary())
                        .max(BigDecimal::compareTo)
                        .ifPresent(val -> System.out.println("Maior Salario de Analista: " + val));

                System.out.println("------------------------------------------------------------");

                System.out.println("3 maiores salarios de Desenvolvedor");
                people.stream()
                        .filter(person -> person.getProfessional().getName().equals("DESENVOLVEDOR"))
                        .limit(3)
                        .map(person -> person.getProfessional().getSalary())
                        .sorted(Comparator.naturalOrder() )
                        .forEach(System.out::println);

                System.out.println("------------------------------------------------------------");

                System.out.println("Os 3 primeiros nomes iniciados por B e K:");
                System.out.println();
                people.stream()
                        .filter(person -> person.getName().toLowerCase().startsWith("b") ||
                                person.getName().toLowerCase().startsWith("k"))
                        .limit(3)
                        .forEach(System.out::println);

                System.out.println();

                System.out.println("Os 3 primeiros nomes terminados por D e E:");
                System.out.println();
                people.stream()
                        .filter(person -> person.getName().toLowerCase().endsWith("d") ||
                                person.getName().toLowerCase().endsWith("e"))
                        .limit(3)
                        .forEach(System.out::println);

                System.out.println("------------------------------------------------------------");

                System.out.println("Os 6 maiores salarios Masculinos");
                people.stream()
                        .filter(person -> person.getSex().equals(SexEnum.MALE))
                        .limit(6)
                        .map(person -> person.getProfessional().getSalary())
                        .sorted(Comparator.naturalOrder())
                        .forEach(System.out::println);

                System.out.println();
                System.out.println("Os 6 maiores salarios Femininos");
                people.stream()
                        .filter(person -> person.getSex().equals(SexEnum.FEMALE))
                        .limit(6)
                        .map(person -> person.getProfessional().getSalary())
                        .sorted(Comparator.naturalOrder())
                        .forEach(System.out::println);

                System.out.println("------------------------------------------------------------");

                System.out.println("Grupo de profissões e seus respectivos profissionais:");
                var profissionals = getPeople().stream()
                        .map(p -> p.getProfessional().getName())
                        .distinct()
                        .collect(Collectors.toList());
                profissionals.forEach(System.out::println);

                var hashMap = new HashMap<String, List<Person>>();
                profissionals.forEach(p -> {
                                hashMap.put(p, getPeople().stream()
                                        .filter(person -> person.getProfessional().getName().equals(p))
                                        .collect(Collectors.toList())
                                );
                        });
                hashMap.forEach((key, value) -> System.out.println("Chave: " + key + " - Valor: " + value));


                System.out.println("------------------------------------------------------------");

                System.out.println("Desenvolvedora com 10% de aumento:");
                people.stream()
                        .filter(person -> person.getProfessional().getName().equals("DESENVOLVEDOR") &&
                                person.getSex().equals(SexEnum.FEMALE))
                        .map(person -> person.getProfessional().getSalary()
                                .add(person.getProfessional().getSalary()
                                .multiply(new BigDecimal("0.1"))))
                        .forEach(System.out::println);

                System.out.println("------------------------------------------------------------");

                System.out.println("3 maiores salarios de Gerente");
                people.stream()
                        .filter(person -> person.getProfessional().getName().equals("GERENTE"))
                        .limit(3)
                        .map(person -> person.getProfessional().getSalary())
                        .sorted(Comparator.reverseOrder() )
                        .forEach(System.out::println);

                System.out.println("------------------------------------------------------------");

        }

        private static List<Person> getPeople(){
                return
                        List.of(
                                new Person("Faizah Ballard", SexEnum.MALE, new Professional(1, "DESENVOLVEDOR", BigDecimal.valueOf(3500.00))),
                                new Person("Alisha Harmon", SexEnum.FEMALE, new Professional(3, "ANALISTA", BigDecimal.valueOf(2500.00))),
                                new Person("Rumaysa Schwartz", SexEnum.FEMALE, new Professional(1, "DESENVOLVEDOR", BigDecimal.valueOf(1500.00))),
                                new Person("Kobi Love", SexEnum.MALE, new Professional(1, "DESENVOLVEDOR", BigDecimal.valueOf(1800.00))),
                                new Person("Ralph Stokes", SexEnum.MALE, new Professional(1, "DESENVOLVEDOR", BigDecimal.valueOf(6500.00))),
                                new Person("Emmanuel Hammond", SexEnum.MALE, new Professional(1, "DESENVOLVEDOR", BigDecimal.valueOf(4500.00))),
                                new Person("Anthony Bird", SexEnum.MALE, new Professional(1, "DESENVOLVEDOR", BigDecimal.valueOf(4500.00))),
                                new Person("Jerome Frye", SexEnum.MALE, new Professional(1, "DESENVOLVEDOR", BigDecimal.valueOf(7500.00))),
                                new Person("Sama Keith", SexEnum.MALE, new Professional(1, "DESENVOLVEDOR", BigDecimal.valueOf(8500.00))),
                                new Person("Alanna Sharp", SexEnum.FEMALE, new Professional(1, "DESENVOLVEDOR", BigDecimal.valueOf(3500.00))),
                                new Person("Husna Suarez", SexEnum.FEMALE, new Professional(1, "DESENVOLVEDOR", BigDecimal.valueOf(8500.00))),
                                new Person("Alisha Pope", SexEnum.FEMALE, new Professional(1, "DESENVOLVEDOR", BigDecimal.valueOf(83500.00))),
                                new Person("Toni Rawlings", SexEnum.MALE, new Professional(1, "DESENVOLVEDOR", BigDecimal.valueOf(32500.00))),
                                new Person("Piotr Harwood", SexEnum.MALE, new Professional(1, "DESENVOLVEDOR", BigDecimal.valueOf(13500.00))),
                                new Person("Roy Dean", SexEnum.MALE, new Professional(1, "DESENVOLVEDOR", BigDecimal.valueOf(23500.00))),
                                new Person("Chelsea Molina", SexEnum.FEMALE, new Professional(1, "DESENVOLVEDOR", BigDecimal.valueOf(3500.00))),
                                new Person("Michelle Schneider", SexEnum.FEMALE, new Professional(1, "DESENVOLVEDOR", BigDecimal.valueOf(2500.00))),
                                new Person("Joshua Graham", SexEnum.MALE, new Professional(1, "DESENVOLVEDOR", BigDecimal.valueOf(1500.00))),
                                new Person("Orlando Dennis", SexEnum.MALE, new Professional(1, "DESENVOLVEDOR", BigDecimal.valueOf(1800.00))),
                                new Person("Emily-Rose Castro", SexEnum.MALE, new Professional(1, "DESENVOLVEDOR", BigDecimal.valueOf(6500.00))),
                                new Person("Joshua Graham", SexEnum.MALE, new Professional(1, "DESENVOLVEDOR", BigDecimal.valueOf(4500.00))),
                                new Person("Beulah Laing", SexEnum.MALE, new Professional(1, "DESENVOLVEDOR", BigDecimal.valueOf(4500.00))),
                                new Person("Joy Perkins", SexEnum.MALE, new Professional(1, "DESENVOLVEDOR", BigDecimal.valueOf(7500.00))),
                                new Person("Kevin Herring", SexEnum.MALE, new Professional(1, "DESENVOLVEDOR", BigDecimal.valueOf(8500.00))),
                                new Person("Anaya Portillo", SexEnum.FEMALE, new Professional(1, "DESENVOLVEDOR", BigDecimal.valueOf(3500.00))),
                                new Person("Dalia Bowler", SexEnum.FEMALE, new Professional(1, "DESENVOLVEDOR", BigDecimal.valueOf(8500.00))),
                                new Person("Joy Perkins", SexEnum.MALE, new Professional(1, "DESENVOLVEDOR", BigDecimal.valueOf(983500.00))),
                                new Person("Nikodem Peralta", SexEnum.MALE, new Professional(1, "DESENVOLVEDOR", BigDecimal.valueOf(32500.00))),
                                new Person("Vikram Sharpe", SexEnum.MALE, new Professional(1, "DESENVOLVEDOR", BigDecimal.valueOf(13500.00))),
                                new Person("Kenya Hyde", SexEnum.FEMALE, new Professional(1, "DESENVOLVEDOR", BigDecimal.valueOf(23500.00))),
                                new Person("Mahnoor Eaton", SexEnum.MALE, new Professional(1, "DESENVOLVEDOR", BigDecimal.valueOf(3500.00))),
                                new Person("Kennedy Craft", SexEnum.MALE, new Professional(2, "GERENTE", BigDecimal.valueOf(6500.00))),
                                new Person("Dewi Hayward", SexEnum.MALE, new Professional(2, "GERENTE", BigDecimal.valueOf(4500.00))),
                                new Person("Ruben Gomez", SexEnum.MALE, new Professional(2, "GERENTE", BigDecimal.valueOf(1500.00))),
                                new Person("Winifred Patel", SexEnum.MALE, new Professional(2, "GERENTE", BigDecimal.valueOf(2500.00))),
                                new Person("Kevin Herring", SexEnum.MALE, new Professional(2, "GERENTE", BigDecimal.valueOf(3800.00))),
                                new Person("Mariah Odom", SexEnum.FEMALE, new Professional(2, "GERENTE", BigDecimal.valueOf(3590.00))),
                                new Person("Khadija Hopkins", SexEnum.FEMALE, new Professional(2, "GERENTE", BigDecimal.valueOf(8500.00))),
                                new Person("Dennis Blaese", SexEnum.MALE, new Professional(2, "GERENTE", BigDecimal.valueOf(983580.00))),
                                new Person("Chloe Collins", SexEnum.MALE, new Professional(2, "GERENTE", BigDecimal.valueOf(32530.00))),
                                new Person("Kaia Bouvet", SexEnum.MALE, new Professional(2, "GERENTE", BigDecimal.valueOf(13500.00))),
                                new Person("Maisie Oneill", SexEnum.FEMALE, new Professional(2, "GERENTE", BigDecimal.valueOf(23500.00))),
                                new Person("Lilliana Davison", SexEnum.FEMALE, new Professional(2, "GERENTE", BigDecimal.valueOf(3510.00))),
                                new Person("Bryn Ellwood", SexEnum.MALE, new Professional(3, "ANALISTA", BigDecimal.valueOf(6500.00))),
                                new Person("Benjamin Prentice", SexEnum.MALE, new Professional(3, "ANALISTA", BigDecimal.valueOf(4502.00))),
                                new Person("Malia Povey", SexEnum.FEMALE, new Professional(3, "ANALISTA", BigDecimal.valueOf(2501.00))),
                                new Person("Lidia Walters", SexEnum.FEMALE, new Professional(3, "ANALISTA", BigDecimal.valueOf(3510.00))),
                                new Person("Kaison Brooks", SexEnum.MALE, new Professional(3, "ANALISTA", BigDecimal.valueOf(510.00))),
                                new Person("Billy Gomez", SexEnum.MALE, new Professional(3, "ANALISTA", BigDecimal.valueOf(6510.00))),
                                new Person("Iolo Horne", SexEnum.MALE, new Professional(3, "ANALISTA", BigDecimal.valueOf(9510.00))),
                                new Person("Boris Mcgrath", SexEnum.MALE, new Professional(3, "ANALISTA", BigDecimal.valueOf(283100.00))),
                                new Person("Ebonie Bowden", SexEnum.FEMALE, new Professional(3, "ANALISTA", BigDecimal.valueOf(12510.00))),
                                new Person("Will Hamilton", SexEnum.MALE, new Professional(3, "ANALISTA", BigDecimal.valueOf(3510.00))),
                                new Person("Arielle Yu", SexEnum.FEMALE, new Professional(3, "ANALISTA", BigDecimal.valueOf(25520.00))),
                                new Person("Roosevelt Fraser", SexEnum.FEMALE, new Professional(3, "ANALISTA", BigDecimal.valueOf(4520.00)))
                        );
        }
}